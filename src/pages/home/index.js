import React, { useEffect, useState } from 'react'
import {
    Container,
    Header
} from 'semantic-ui-react';
import HeaderBlurb from '../../components/HeaderBlurb/HeaderBlurb';
import FeatureBlock from './FeatureBlock';

function Home() {
    const [appState, setAppState] = useState({
        loading: false,
        results: [],
    })

    let [features, setFeatures] = useState([])

    // gets initial array of content
    useEffect(() => {
        setAppState({ loading: true, results: [] })
        const apiURL = 'https://api.buildable.dev/trigger/v2/test-de224d12-d7ea-4bd1-8be2-28cb4e4ebd91';
        fetch(apiURL, {
            method: "POST",
            body: JSON.stringify({
                databaseID: "d70b859b08b14ae3b30b73f90c6ee0db"

            }),
            headers: {
                "Content-Type": "application/json",
            }
        })
            .then((res) => res.json())
            .then((results) => {
                console.log(results.results)
                setAppState({ loading: false, results: results.results })
            })
    }, [setAppState]);

    // gets features from array
    useEffect(() => {
        function filterByFeature(obj){
            if (obj.properties["Type"].select.name === "Features"){
                return obj
            }
        }
        let newArray = appState.results.filter(filterByFeature);
        setFeatures(newArray)
    }, [appState.results]);

    return (
        <Container text>
            <HeaderBlurb />
            <p>This is an OSC remote for iPhone, iPad, and iPod Touch to control Hog 4 consoles. It allows you to quickly put fixtures into palettes and groups and clear the programmer or knockout the last selected group. You also have playback options like going lists and scenes and releasing lists and scenes.</p>
            <Header as="h2">Features</Header>
            {features.map((entry, index) => (
                <FeatureBlock
                key={index}
                name={entry.properties["Name"].title[0].text.content}
                shortDescription={entry.properties["shortDescription"].rich_text[0].plain_text}
                imageURL={entry.properties["ImageURL"].url}/>
             ))}
        </Container>
    )
}

export default Home;