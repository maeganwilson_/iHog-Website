import React from 'react';
import { Grid, Header, Image, Segment } from 'semantic-ui-react';

function FeatureBlock(props) {
    return (
        <Segment basic>
            <Header>{props.name}</Header>
            <Grid>
                <Grid.Column width={9}>
                    {props.shortDescription}
                </Grid.Column>
                <Grid.Column floated='right' width={9}>
                    <Image src={props.imageURL}/>
                </Grid.Column>
            </Grid>
        </Segment>
    )
}

export default FeatureBlock;