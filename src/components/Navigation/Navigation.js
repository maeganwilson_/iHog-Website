import React from 'react';
import {
    NavLink
} from 'react-router-dom';
import { Image, Menu } from 'semantic-ui-react';
import app_icon from '../../images/app_icon.png';
import './Navigation.css';

const Navigation = () => (
    <Menu stackable>
        <Menu.Item header>
            <Image src={app_icon} size="mini" rounded className="MenuImage"/>
            iHog
        </Menu.Item>
        <Menu.Item name="home" as={NavLink} to="/home">
            Home
        </Menu.Item>
        <Menu.Item name="faq" as={NavLink} to="/faq">
            F.A.Q.
        </Menu.Item>
        <Menu.Item name="privacy_policy" as={NavLink} to="/privacypolicy">
            Privacy Policy
        </Menu.Item>
        <Menu.Item name="release_notes" as={NavLink} to="/releases">
            Release Notes
        </Menu.Item>
    </Menu>
);

export default Navigation;