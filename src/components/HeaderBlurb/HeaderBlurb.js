import React from 'react'
import {
    Grid,
    GridColumn,
    Header,
    Image,
    Segment
} from 'semantic-ui-react';
import appStore from "../../images/download_app_store.svg";

const HeaderBlurb = () => {
    return (
        <Segment>
            <Grid verticalAlign="middle">
            <GridColumn width={9}>
                <Header as="h1">iHog</Header>
                <p>An OSC remote app to control your Hog 4 console!</p>
            </GridColumn>
            <GridColumn floated='right' width={3}>
                <Image src={appStore} href="https://apps.apple.com/us/app/ihog-osc-remote-for-hog-4/id1487580623?itsct=apps_box_link&itscg=30200"/>
            </GridColumn>
        </Grid>
        </Segment>
    )
}

export default HeaderBlurb