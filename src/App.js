import React from 'react';
import {
    BrowserRouter as Router,
    Route,
    Switch
} from 'react-router-dom';
import { Container } from 'semantic-ui-react';

import Navigation from './components/Navigation/Navigation';
import Home from './pages/Home';

function App() {
    return (
        <Router>
            <Container>
                <Navigation />
            <Switch>
                <Route path="/home" exact>
                    <Home />
                </Route>
                <Route path="/faq" exact>
                    <Home />
                </Route>
                <Route path="/privacypolicy" exact>
                    <Home />
                </Route>
                <Route path="/releases" exact>
                    <Home />
                </Route>
            </Switch>

            </Container>
        </Router>
    );
}

export default App;
